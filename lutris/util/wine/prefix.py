"""Wine prefix management"""
import os
import time
from getpass import getuser
from typing import Dict, List

from lutris.util import joypad, system
from lutris.util.display import DISPLAY_MANAGER
from lutris.util.log import logger
from lutris.util.wine.registry import WineRegistry
from lutris.util.wine.wine import get_overrides_env

DESKTOP_KEYS: List[str] = ["Desktop", "Personal", "My Music", "My Videos", "My Pictures"]
DESKTOP_FOLDERS: List[str] = ["Desktop", "My Documents", "My Music", "My Videos", "My Pictures"]
DESKTOP_XDG: List[str] = ["DESKTOP", "DOCUMENTS", "MUSIC", "VIDEOS", "PICTURES"]

HKEY_CURRENT_USER: str = "HKEY_CURRENT_USER"
HKEY_SOFTWARE: str = HKEY_CURRENT_USER + "/Software"

HKEY_WINE: str = HKEY_SOFTWARE + "/Wine"
HKEY_WINE_DIRECT_3D: str = HKEY_WINE + "/Direct3D"
HKEY_WINE_DIRECT_INPUT: str = HKEY_WINE + "/DirectInput"
HKEY_WINE_DLL_OVERRIDES: str = HKEY_WINE + "/DllOverrides"
HKEY_WINE_DBG: str = HKEY_WINE + "/WineDbg"
HKEY_WINE_DRIVERS: str = HKEY_WINE + "/Drivers"
HKEY_WINE_X11_DRIVER: str = HKEY_WINE + "/X11 Driver"

DEFAULT_SETTINGS = {
    HKEY_WINE_DIRECT_3D: {
    },
    HKEY_WINE_DLL_OVERRIDES: {
        'mshtml': '',
        'steamwebhelper.exe': '',
        'winedbg.exe': '',
        'winemenubuilder.exe': '',
    },
    HKEY_WINE_DBG: {
        # DWORD value.
        # Set this to "0" to let applications handle exceptions themselves.
        # winedbg then only catches exceptions that are not handled by the app,
        # which makes debugging a bit easier.
        # Default value is "1" (enabled)
        'BreakOnFirstChance': 0,
        # DWORD value.
        # Set this to "0" to disable the GUI crash dialog.
        # Default value is "1" (enabled)
        'ShowCrashDialog': 0,
    },
    HKEY_WINE_X11_DRIVER: {
        # Set this to "N" to disallow the window manager to decorate created windows.
        'Decorated': 'N',
        # Set this to "N" to disallow the window manager to control created windows.
        'Managed': 'Y',
        # Set this to "Y" to force full-screen windows to capture the mouse.
        'GrabFullscreen': 'Y',
        # PS: Disable Cliend Side to increase performace.
        # Set this to "N" if you don't want to use the Dib engine to render client side windows.
        'ClientSideGraphics': 'N',
        # Set this to "N" if you don't want to use the Render extension to render client side fonts.
        'ClientSideWithRender': 'N',
        # Set this to "N" to disable font anti-aliasing when X-Render extension is present.
        'ClientSideAntiAliasWithRender': 'N',
        # Set this to "N" to disable font anti-aliasing when X-Render extension is not present or disabled.
        'ClientSideAntiAliasWithCore': 'N',
    }
}


class WinePrefixManager:
    """Class to allow modification of Wine prefixes without the use of Wine"""

    def __init__(self, wine_executable: str, arch: str, prefix: str, home: str) -> None:
        self.wine_executable: str = wine_executable
        self.wineboot_executable: str = os.path.join(os.path.dirname(self.wine_executable), "wineboot")
        self.arch: str = arch
        self.prefix: str = prefix
        self.user_registry: str = os.path.join(self.prefix, 'user.reg')
        self.home: str = home

    def bootstrap(self) -> None:
        if not os.path.exists(self.user_registry):
            self._wineboot()

        self._init_settings()
        self._home_integration()

    def _wineboot(self) -> None:
        wineenv = {
            "WINEARCH": self.arch,
            "WINEPREFIX": self.prefix,
            "WINEDLLOVERRIDES": self._transform_dll_overrides_to_environment(DEFAULT_SETTINGS[HKEY_WINE_DLL_OVERRIDES]),
        }

        os.makedirs(self.prefix, exist_ok=True)
        self._wineboot_init(wineenv)

        for loop_index in range(50):
            time.sleep(0.25)
            if os.path.exists(self.user_registry):
                break
            if loop_index == 20:
                logger.warning("Wine prefix creation is taking longer than expected...")

        if not os.path.exists(self.user_registry):
            raise ValueError("No user.reg found after prefix creation. Prefix might not be valid")
        logger.info("%s Prefix created in %s", self.arch, self.prefix)

    def _wineboot_init(self, wineenv: Dict[str, str]):
        system.execute([self.wineboot_executable, '--init'], wineenv)

    def _wineboot_shutdown(self, wineenv: Dict[str, str]):
        system.execute([self.wineboot_executable, '--shutdown'], wineenv)

    @staticmethod
    def _transform_dll_overrides_to_environment(dll_overrides: Dict[str, str]):
        return get_overrides_env(dll_overrides)

    def _init_settings(self):
        for key, settings in DEFAULT_SETTINGS.items():
            for setting, value in settings.items():
                self.set_registry_key(key, setting, value)

    def _home_integration(self):
        """Wine home integration"""
        user = getuser()
        user_dir = os.path.join(self.prefix, "drive_c/users/", user)
        desktop_dir = os.path.expanduser(self.home)

        try:
            if os.path.exists(user_dir):
                # Replace or restore desktop integration symlinks
                for i, item in enumerate(DESKTOP_FOLDERS):
                    path = os.path.join(user_dir, item)

                    if os.path.islink(path):
                        os.unlink(path)
                    src_path = os.path.join(desktop_dir, item)
                    os.makedirs(src_path, exist_ok=True)
                    os.symlink(src_path, path)
        except OSError as ex:
            logger.error("Failed to setup desktop integration, the prefix may not be valid.")
            logger.exception(ex)

    def set_virtual_desktop(self, enabled):
        """Enable or disable wine virtual desktop.
        The Lutris virtual desktop is refered to as 'WineDesktop', in Wine the
        virtual desktop name is 'default'.
        """
        path = HKEY_CURRENT_USER + "/Software/Wine/Explorer"
        if enabled:
            self.set_registry_key(path, "Desktop", "WineDesktop")
            default_resolution = "x".join(DISPLAY_MANAGER.get_current_resolution())
            logger.debug(
                "Enabling wine virtual desktop with default resolution of %s",
                default_resolution,
            )
            self.set_registry_key(
                HKEY_CURRENT_USER + "/Software/Wine/Explorer/Desktops",
                "WineDesktop",
                default_resolution,
            )
        else:
            self.clear_registry_key(path)

    def set_desktop_size(self, desktop_size):
        """Sets the desktop size if one is given but do not reset the key if
        one isn't.
        """
        path = HKEY_CURRENT_USER + "/Software/Wine/Explorer/Desktops"
        if desktop_size:
            self.set_registry_key(path, "WineDesktop", desktop_size)

    def use_xvid_mode(self, enabled):
        """Set this to "Y" to allow wine switch the resolution using XVidMode extension."""
        self.set_x11_driver_setting('UseXVidMode', enabled)

    def configure_joypads(self):
        joypads = joypad.get_joypads()
        key = HKEY_CURRENT_USER + "/Software/Wine/DirectInput/Joysticks"
        self.clear_registry_key(key)
        for device, joypad_name in joypads:
            if "event" in device:
                disabled_joypad = "{} (event)".format(joypad_name)
            else:
                disabled_joypad = "{} (js)".format(joypad_name)
            self.set_registry_key(key, disabled_joypad, "disabled")

    # =========================== * Core Services * ========================== #
    @staticmethod
    def get_key_path(key):
        if key.startswith(HKEY_CURRENT_USER):
            return key[len(HKEY_CURRENT_USER) + 1:]
        raise ValueError(
            "The key {} is currently not supported by WinePrefixManager".format(key)
        )

    def get_registry_path(self, key):
        """Matches registry keys to a registry file

        Currently, only HKEY_CURRENT_USER keys are supported.
        """
        if key.startswith(HKEY_CURRENT_USER):
            return os.path.join(self.prefix, "user.reg")
        raise ValueError("Unsupported key '{}'".format(key))

    def get_registry_key(self, key, subkey):
        registry = WineRegistry(self.get_registry_path(key))
        return registry.query(self.get_key_path(key), subkey)

    def set_registry_key(self, key, subkey, value):
        registry = WineRegistry(self.get_registry_path(key))
        registry.set_value(self.get_key_path(key), subkey, value)
        registry.save()

    def clear_registry_key(self, key):
        registry = WineRegistry(self.get_registry_path(key))
        registry.clear_key(self.get_key_path(key))
        registry.save()

    def clear_registry_subkeys(self, key, subkeys):
        registry = WineRegistry(self.get_registry_path(key))
        registry.clear_subkeys(self.get_key_path(key), subkeys)
        registry.save()

    # ========================== * Extra Services * ========================== #
    def set_dll_overrides_setting(self, dll: str, mode: str):
        if mode.startswith("dis"):
            mode = ""
        if mode not in ("builtin", "native", "builtin,native", "native,builtin", ""):
            logger.error("DLL override '%s' mode is not valid", mode)
            return
        self.set_registry_key(HKEY_WINE_DLL_OVERRIDES, dll, mode)

    def set_x11_driver_setting(self, setting: str, enabled: bool) -> None:
        self.set_registry_key(HKEY_WINE_X11_DRIVER, setting, 'Y' if enabled else 'N')

    def set_wine_dbg_setting(self, setting: str, enabled: bool) -> None:
        self.set_registry_key(HKEY_WINE_DBG, setting, 1 if enabled else 0)
