"""Module to deal with various aspects of displays"""
import os
import subprocess

from lutris.util import system
from lutris.util.graphics.xrandr import LegacyDisplayManager
from lutris.util.log import logger


def restore_gamma():
    """Restores gamma to a normal level."""
    xgamma_path = system.find_executable("xgamma")
    try:
        subprocess.Popen([xgamma_path, "-gamma", "1.0"])
    except (FileNotFoundError, TypeError):
        logger.warning("xgamma is not available on your system")
    except PermissionError:
        logger.warning("you do not have permission to call xgamma")


def _get_graphics_adapters():
    """Return the list of graphics cards available on a system

    Returns:
        list: list of tuples containing PCI ID and description of the display controller
    """
    lspci_path = system.find_executable("lspci")
    dev_subclasses = ["VGA", "XGA", "3D controller", "Display controller"]
    if not lspci_path:
        logger.warning("lspci is not available. List of graphics cards not available")
        return []
    return [
        (pci_id, device_desc.split(": ")[1])
        for pci_id, device_desc in [
            line.split(maxsplit=1)
            for line in system.execute(lspci_path).split("\n")
            if any(subclass in line for subclass in dev_subclasses)
        ]
    ]


DISPLAY_MANAGER = LegacyDisplayManager()
USE_DRI_PRIME = len(_get_graphics_adapters()) > 1


def get_compositor_commands():
    """Nominated for the worst function in lutris"""
    start_compositor = None
    stop_compositor = None
    desktop_session = os.environ.get("DESKTOP_SESSION")

    if desktop_session == "xfce":
        start_compositor, stop_compositor = [
            "xfconf-query --channel=xfwm4 --property=/general/use_compositing --set=true",
            "xfconf-query --channel=xfwm4 --property=/general/use_compositing --set=false"
        ]
    elif desktop_session == "plasma":
        start_compositor, stop_compositor = [
            "qdbus org.kde.KWin /Compositor org.kde.kwin.Compositing.suspend",
            "qdbus org.kde.KWin /Compositor org.kde.kwin.Compositing.resume"
        ]
    elif desktop_session == "mate":
        start_compositor, stop_compositor = [
            "gsettings set org.mate.Marco.general compositing-manager true",
            "gsettings set org.mate.Marco.general compositing-manager false"
        ]
    elif desktop_session == "deepin":
        start_compositor, stop_compositor = [
                                                "dbus-send --session --dest=com.deepin.WMSwitcher --type=method_call "
                                                "/com/deepin/WMSwitcher com.deepin.WMSwitcher.RequestSwitchWM"
                                            ] * 2
    return start_compositor, stop_compositor
