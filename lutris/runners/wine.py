"""Wine runner"""
# pylint: disable=too-many-arguments
import os
import shlex
import shutil
from getpass import getuser
from typing import Optional

from lutris import runtime
from lutris.exceptions import GameConfigError
from lutris.gui.dialogs import FileDialog
from lutris.runners.commands.wine import (
    winecfg,
    wineexec,
    winekill,
    winetricks,
)
from lutris.runners.runner import Runner
from lutris.settings import RUNTIME_DIR, SANDBOX_DIR
from lutris.util import system
from lutris.util.display import DISPLAY_MANAGER
from lutris.util.graphics.vkquery import is_vulkan_supported
from lutris.util.jobs import thread_safe_call
from lutris.util.log import logger
from lutris.util.strings import parse_version, split_arguments
from lutris.util.wine import dxvk
from lutris.util.wine.prefix import (
    HKEY_WINE_DIRECT_3D,
    HKEY_WINE_DIRECT_INPUT,
    HKEY_WINE_DRIVERS,
    WinePrefixManager
)
from lutris.util.wine.wine import (
    detect_arch,
    display_vulkan_error,
    esync_display_limit_warning,
    esync_display_version_warning,
    get_default_version,
    get_overrides_env,
    get_real_executable,
    get_system_wine_version,
    get_wine_versions,
    is_esync_limit_set,
    is_version_esync,
    WINE_DIR,
    WINE_PATHS
)
from lutris.util.wine.x360ce import X360ce

MIN_SAFE_VERSION = "4.0"  # Wine installers must run with at least this version


class wine(Runner):
    description = "Runs Windows games"
    human_name = "Wine"
    platforms = ["Windows"]
    multiple_versions = True

    reg_keys = {
        "Audio": HKEY_WINE_DRIVERS,
        "MouseWarpOverride": HKEY_WINE_DIRECT_INPUT,
        "OffscreenRenderingMode": HKEY_WINE_DIRECT_3D,
        "StrictDrawOrdering": HKEY_WINE_DIRECT_3D,
        "SampleCount": HKEY_WINE_DIRECT_3D,
        "Desktop": "MANAGED",
        "WineDesktop": "MANAGED",
        "UseXVidMode": "MANAGED",
    }

    core_processes = (
        "explorer.exe",
        "plugplay.exe",
        "rpcss.exe",
        "rundll32.exe",
        "services.exe",
        "wineboot.exe",
        "winedevice.exe",
        "winemenubuilder.exe",
    )

    def __init__(self, config=None):
        super(wine, self).__init__(config)
        self.dll_overrides = {}

        def get_wine_version_choices():
            version_choices = [("Custom (select executable below)", "custom")]
            labels = {
                "winehq-devel": "WineHQ devel ({})",
                "winehq-staging": "WineHQ staging ({})",
                "wine-development": "Wine Development ({})",
                "system": "System ({})",
            }
            versions = get_wine_versions()
            for version in versions:
                if version in labels.keys():
                    version_number = get_system_wine_version(WINE_PATHS[version])
                    label = labels[version].format(version_number)
                else:
                    label = version
                version_choices.append((label, version))
            return version_choices

        def dxvk_choices(manager_class):
            version_choices = [
                ("Manual", "manual"),
                (manager_class.DXVK_LATEST, manager_class.DXVK_LATEST),
            ]
            return version_choices

        def get_dxvk_choices():
            return dxvk_choices(dxvk.DXVKManager)

        def esync_limit_callback(widget, option, config):
            limits_set = is_esync_limit_set()
            wine_path = self._get_path_for_version(config["version"])
            wine_ver = is_version_esync(wine_path)
            response = True

            if not wine_ver:
                response = thread_safe_call(esync_display_version_warning)

            if not limits_set:
                thread_safe_call(esync_display_limit_warning)
                response = False

            return widget, option, response

        def dxvk_vulkan_callback(widget, option, config):
            response = is_vulkan_supported() or thread_safe_call(display_vulkan_error)
            return widget, option, response

        self.game_options = [
            {
                "option": "exe",
                "type": "file",
                "label": "Executable",
                "default": self.system_config.get("game_path") if self.config else None,
                "relative_to": self.system_config.get("game_path") + "/" if self.config else None,
                "help": "The game's main EXE file",
            },
            {
                "option": "args",
                "type": "string",
                "label": "Arguments",
                "help": "Windows command line arguments used when launching the game",
                "validator": shlex.split
            },
            {
                "option": "working_dir",
                "type": "directory_chooser",
                "label": "Working directory",
                "help": (
                    "The location where the game is run from.\n"
                    "By default, Lutris uses the directory of the "
                    "executable."
                ),
            },
            {
                "option": "windows_version",
                "type": "choice",
                "label": "Windows version",
                "choices": [("Windows XP", "winxp"), ("Windows 7", "win7")],
                "default": "win7",
                "help": "Windows version",
            },
            {
                "option": "arch",
                "type": "choice",
                "label": "Prefix architecture",
                "choices": [("64-bit", "win64"), ("32-bit", "win32")],
                "default": "win64",
                "help": "The architecture of the Windows environment",
            },
            {
                "option": "save_directory",
                "type": "directory_chooser",
                "default": self.prefix_drive_c_path if self.config else None,
                "label": "Game save directory",
                "help": (
                    "Game save directory to backup on reset"
                )
            },
            {
                "option": "secondary_save_directory",
                "type": "directory_chooser",
                "default": self.prefix_drive_c_path if self.config else None,
                "label": "Secondary game save directory",
                "help": (
                    "Secondary game save directory to backup on reset"
                )
            },
            {
                "option": "save_registry",
                "type": "string",
                "label": "Game registry key",
                "help": (
                    "Game save registries to backup on reset"
                )
            }
        ]

        self.runner_options = [
            {
                "option": "version",
                "label": "Wine version",
                "type": "choice",
                "choices": get_wine_version_choices,
                "default": get_default_version(),
                "help": (
                    "The version of Wine used to launch the game.\n"
                    "Using the last version is generally recommended, "
                    "but some games work better on older versions."
                ),
            },
            {
                "option": "custom_wine_path",
                "label": "Custom Wine executable",
                "type": "file",
                "advanced": True,
                "help": (
                    "The Wine executable to be used if you have "
                    'selected "Custom" as the Wine version.'
                ),
            },
            {
                "option": "dxvk",
                "label": "Enable DXVK",
                "type": "extended_bool",
                "callback": dxvk_vulkan_callback,
                "callback_on": True,
                "active": True,
                "help": (
                    "Use DXVK to increase compatibility and performance "
                    "in Direct3D 11 and 10 applications by translating "
                    "their calls to Vulkan."
                ),
            },
            {
                "option": "dxvk_version",
                "label": "DXVK version",
                "advanced": True,
                "type": "choice_with_entry",
                "choices": get_dxvk_choices,
                "default": dxvk.DXVKManager.DXVK_LATEST,
            },
            {
                "option": "esync",
                "label": "Enable Esync",
                "type": "extended_bool",
                "callback": esync_limit_callback,
                "callback_on": True,
                "active": True,
                "help": (
                    "Enable eventfd-based synchronization (esync). "
                    "This will increase performance in applications "
                    "that take advantage of multi-core processors."
                ),
            },
            {
                "option": "x360ce-path",
                "label": "Path to the game's executable, for x360ce support",
                "type": "directory_chooser",
                "help": "Locate the path for the game's executable for x360 support",
                "advanced": True,
            },
            {
                "option": "x360ce-dinput",
                "label": "x360ce dinput 8 mode",
                "type": "bool",
                "default": False,
                "help": "Configure x360ce with dinput8.dll, required for some games",
                "advanced": True,
            },
            {
                "option": "x360ce-xinput9",
                "label": "x360ce xinput 9.1.0 mode",
                "type": "bool",
                "default": False,
                "help": "Configure x360ce with xinput9_1_0.dll, required for some newer games",
                "advanced": True,
            },
            {
                "option": "dumbxinputemu",
                "label": "Use Dumb xinput Emulator (experimental)",
                "type": "bool",
                "default": False,
                "help": "Use the dlls from kozec/dumbxinputemu",
                "advanced": True,
            },
            {
                "option": "xinput-arch",
                "label": "Xinput architecture",
                "type": "choice",
                "choices": [
                    ("Same as wine prefix", ""),
                    ("32 bit", "win32"),
                    ("64 bit", "win64"),
                ],
                "default": "",
                "advanced": True,
            },
            {
                "option": "Desktop",
                "label": "Windowed (virtual desktop)",
                "type": "bool",
                "default": False,
                "help": (
                    "Run the whole Windows desktop in a window.\n"
                    "Otherwise, run it fullscreen.\n"
                    "This corresponds to Wine's Virtual Desktop option."
                ),
            },
            {
                "option": "WineDesktop",
                "label": "Virtual desktop resolution",
                "type": "choice_with_entry",
                "choices": DISPLAY_MANAGER.get_resolutions,
                "help": "The size of the virtual desktop in pixels.",
            },
            {
                "option": "MouseWarpOverride",
                "label": "Mouse Warp Override",
                "type": "choice",
                "choices": [
                    ("Enable", "enable"),
                    ("Disable", "disable"),
                    ("Force", "force"),
                ],
                "default": "enable",
                "advanced": True,
                "help": (
                    "Override the default mouse pointer warping behavior\n"
                    "<b>Enable</b>: (Wine default) warp the pointer when the "
                    "mouse is exclusively acquired \n"
                    "<b>Disable</b>: never warp the mouse pointer \n"
                    "<b>Force</b>: always warp the pointer"
                ),
            },
            {
                "option": "OffscreenRenderingMode",
                "label": "Offscreen Rendering Mode",
                "type": "choice",
                "choices": [("FBO", "fbo"), ("BackBuffer", "backbuffer")],
                "default": "fbo",
                "advanced": True,
                "help": (
                    "Select the offscreen rendering implementation.\n"
                    "<b>FBO</b>: (Wine default) Use framebuffer objects "
                    "for offscreen rendering \n"
                    "<b>Backbuffer</b>: Render offscreen render targets "
                    "in the backbuffer."
                ),
            },
            {
                "option": "StrictDrawOrdering",
                "label": "Strict Draw Ordering",
                "type": "choice",
                "choices": [("Enabled", "enabled"), ("Disabled", "disabled")],
                "default": "disabled",
                "advanced": True,
                "help": (
                    "This option ensures any pending drawing operations are "
                    "submitted to the driver, but at a significant performance "
                    'cost. Set to "enabled" to enable. This setting is deprecated '
                    "since wine-2.6 and will likely be removed after wine-3.0. "
                    'Use "csmt" instead.'
                ),
            },
            {
                "option": "UseGLSL",
                "label": "Use GLSL",
                "type": "choice",
                "choices": [("Enabled", "enabled"), ("Disabled", "disabled")],
                "default": "enabled",
                "advanced": True,
                "help": (
                    'When set to "disabled", this disables the use of GLSL for shaders. '
                    "In general disabling GLSL is not recommended, "
                    "only use this for debugging purposes."
                ),
            },
            {
                "option": "SampleCount",
                "label": "Anti-aliasing Sample Count",
                "type": "choice",
                "choices": [
                    ("Auto", "auto"),
                    ("0", "0"),
                    ("2", "2"),
                    ("4", "4"),
                    ("8", "8"),
                    ("16", "16"),
                ],
                "default": "auto",
                "advanced": True,
                "help": (
                    "Override swapchain sample count. It can be used to force enable multisampling "
                    "with applications that otherwise don't support it, like the similar control "
                    "panel setting available with some GPU drivers. This one might work in more "
                    "cases than the driver setting though. "
                    "Not all applications are compatible with all sample counts. "
                ),
            },
            {
                "option": "UseXVidMode",
                "label": "Use XVidMode to switch resolutions",
                "type": "bool",
                "default": False,
                "advanced": True,
                "help": (
                    'Set this to "Y" to allow wine switch the resolution using XVidMode extension.'
                ),
            },
            {
                "option": "Audio",
                "label": "Audio driver",
                "type": "choice",
                "advanced": True,
                "choices": [
                    ("Auto", "auto"),
                    ("ALSA", "alsa"),
                    ("PulseAudio", "pulse"),
                    ("OSS", "oss"),
                ],
                "default": "auto",
                "help": (
                    "Which audio backend to use.\n"
                    "By default, Wine automatically picks the right one "
                    "for your system."
                ),
            },
            {
                "option": "overrides",
                "type": "mapping",
                "label": "DLL overrides",
                "help": "Sets WINEDLLOVERRIDES when launching the game.",
            },
            {
                "option": "show_debug",
                "label": "Output debugging info",
                "type": "choice",
                "choices": [
                    ("Disabled", "-all"),
                    ("Enabled", ""),
                    ("Inherit from environment", "inherit"),
                    ("Show FPS", "+fps"),
                    ("Full (CAUTION: Will cause MASSIVE slowdown)", "+all"),
                ],
                "default": "-all",
                "help": (
                    "Output debugging information in the game log "
                    "(might affect performance)"
                ),
            },
            {
                "option": "autoconf_joypad",
                "type": "bool",
                "label": "Autoconfigure joypads",
                "advanced": True,
                "default": True,
                "help": (
                    "Automatically disables one of Wine's detected joypad "
                    "to avoid having 2 controllers detected"
                ),
            }
        ]

    def prelaunch(self):
        prefix_manager = WinePrefixManager(
            self.get_executable(),
            self.wine_arch,
            self.prefix_path,
            self.wine_home
        )

        prefix_manager.bootstrap()
        self._set_windows_version()
        self._configure_joypads(prefix_manager)
        self._set_regedit_keys(prefix_manager)
        self._secure_save_directory(self.save_directory_path)
        self._secure_save_directory(self.secondary_save_directory_path)
        self._load_registries_data()
        self.setup_x360ce(self.runner_config.get("x360ce-path"))
        self.setup_dxvk()

        return True

    def play(self):
        game_exe = self.game_exe
        arguments = self.game_config.get("args", "")
        launch_info = {"env": self.get_env(os_env=False)}
        using_dxvk = self.runner_config.get("dxvk") or self.runner_config.get("d9vk")

        if using_dxvk:
            # Set this to 1 to enable access to more RAM for 32bit applications
            launch_info["env"]["WINE_LARGE_ADDRESS_AWARE"] = "1"
            if not is_vulkan_supported() and not display_vulkan_error(True):
                return {"error": "VULKAN_NOT_FOUND"}

        if not system.path_exists(game_exe):
            return {"error": "FILE_NOT_FOUND", "file": game_exe}

        if launch_info["env"].get("WINEESYNC") == "1":
            limit_set = is_esync_limit_set()
            wine_ver = is_version_esync(self.get_executable())

            if not limit_set and not wine_ver:
                esync_display_version_warning(True)
                esync_display_limit_warning()
                return {"error": "ESYNC_LIMIT_NOT_SET"}
            if not is_esync_limit_set():
                esync_display_limit_warning()
                return {"error": "ESYNC_LIMIT_NOT_SET"}
            if not wine_ver and not esync_display_version_warning(True):
                return {"error": "NON_ESYNC_WINE_VERSION"}

        command = [self.get_executable()]

        game_exe, args, _working_dir = get_real_executable(game_exe, self.working_dir)
        command.append(game_exe)
        if args:
            command = command + args

        if arguments:
            for arg in split_arguments(arguments):
                command.append(arg)
        launch_info["command"] = command
        return launch_info

    def postexit(self):
        self._save_registries_data()

    # =========================== * Context Menu * =========================== #
    @property
    def context_menu_entries(self):
        """Return the contexual menu entries for wine"""
        menu_entries = [
            ("wineexec", "Run EXE inside wine prefix", self.run_wineexec),
            ("wineconsole", "Wine console", self.run_wineconsole),
            ("wine-regedit", "Wine registry", self.run_regedit),
            ("winekill", "Kill all wine processes", self.run_winekill),
            ("winetricks", "Winetricks", self.run_winetricks),
            ("winecpl", "Wine Control Panel", self.run_winecpl),
            ("remove-wine-prefix", "Remove Wine Prefix", self.remove_wine_prefix)
        ]

        return menu_entries

    def run_wineexec(self, *args):
        """Ask the user for an arbitrary exe file to run in the game's prefix"""
        dlg = FileDialog("Select an EXE or MSI file", default_path=self.game_path)
        filename = dlg.filename
        if not filename:
            return
        self.prelaunch()
        self._run_executable(filename)

    def run_wineconsole(self, *args):
        """Runs wineconsole inside wine prefix."""
        self._run_executable("wineconsole")

    def run_regedit(self, *args):
        """Run regedit in the current context"""
        self.prelaunch()
        self._run_executable("regedit")

    def run_winecpl(self, *args):
        """Execute Wine control panel."""
        self.prelaunch()
        self._run_executable("control")

    def run_winecfg(self, *args):
        """Run winecfg in the current context"""
        self.prelaunch()
        winecfg(
            wine_path=self.get_executable(),
            prefix=self.prefix_path,
            arch=self.wine_arch,
            config=self,
        )

    def run_winetricks(self, *args):
        """Run winetricks in the current context"""
        self.prelaunch()
        winetricks(
            "", prefix=self.prefix_path, wine_path=self.get_executable(), config=self
        )

    def run_winekill(self, *args):
        """Runs wineserver -k."""
        winekill(
            self.prefix_path,
            arch=self.wine_arch,
            wine_path=self.get_executable(),
            env=self.get_env(),
            initial_pids=self._get_pids(),
        )
        return True

    def remove_wine_prefix(self, *args):
        if system.path_exists(self.prefix_path):
            logger.info("Removing prefix '%s' in progress...", self.prefix_path)
            shutil.rmtree(self.prefix_path, True)
            logger.info("Removed prefix in '%s'", self.prefix_path)
        else:
            logger.warn("Could not remove prefix in '%s', because it does not exist", self.prefix_path)

    # =========================== * Game Options * =========================== #
    @property
    def game_exe(self):
        """Return the game's executable's path."""
        executable = self.game_config.get("exe")
        if not executable:
            logger.warning("The game doesn't have an executable")
            return

        if executable and os.path.isabs(executable):
            return executable

        executable = os.path.join(self.default_path, executable)
        if system.path_exists(executable):
            return executable

    @property
    def working_dir(self):
        """Return the working directory to use when running the game."""
        option = self.game_config.get("working_dir")
        if option:
            return option
        if self.game_exe:
            return os.path.dirname(self.game_exe)
        return super(wine, self).working_dir

    @property
    def wine_arch(self):
        """Return the wine architecture.

        Get it from the config or detect it from the prefix"""
        arch = self.game_config.get("arch") or "win64"
        if arch not in ("win32", "win64"):
            arch = detect_arch(self.prefix_path, self.get_executable())
        return arch

    # ============================== * Sandbox * ============================= #
    @property
    def sandbox_path(self) -> str:
        return os.path.join(SANDBOX_DIR, self.config.game_config_id)

    @property
    def prefix_path(self) -> str:
        """Return the absolute path of the Wine prefix"""
        return os.path.join(self.sandbox_path, 'prefix')

    @property
    def prefix_drive_c_path(self) -> str:
        return os.path.join(self.prefix_path, 'drive_c')

    @property
    def wine_home(self) -> str:
        """Return the absolute path of the Wine Desktop path"""
        return os.path.join(self.sandbox_path, 'home')

    @property
    def save_directory_path(self) -> Optional[str]:
        return self._get_save_directory_path('save_directory')

    @property
    def secondary_save_directory_path(self) -> Optional[str]:
        return self._get_save_directory_path('secondary_save_directory')

    def _get_save_directory_path(self, config_key: str):
        save_directory: Optional[str] = self.game_config.get(config_key)
        if not save_directory:
            return

        # Resolve special variables in path.
        save_directory = save_directory.replace('${USER}', os.path.join('users', getuser()))
        save_directory = save_directory.replace('${PUBLIC}', os.path.join('users', 'Public'))

        if not os.path.isabs(save_directory):
            save_directory = os.path.join(self.prefix_drive_c_path, save_directory)

        if not os.path.exists(save_directory):
            logger.warn('Save directory does not exist "%s"', save_directory)

        return save_directory

    @property
    def save_registries_quarantine_file(self) -> str:
        return os.path.join(self.sandbox_path, "save", "save.reg")

    @property
    def registry_data_key(self) -> Optional[str]:
        if self.game_config.get("save_registry"):
            return "HKEY_CURRENT_USER\\Software\\" + self.game_config.get("save_registry")

    def _secure_save_directory(self, save_directory: Optional[str]) -> None:
        if not save_directory:
            return

        quarantine_directory: str = self.save_directory_quarantine_path(save_directory)
        base_quarantine_directory: str = os.path.dirname(quarantine_directory)

        # No backup found, copy all origin content.
        if not os.path.exists(quarantine_directory):
            os.makedirs(base_quarantine_directory, exist_ok=True)
            if os.path.exists(save_directory):
                shutil.move(save_directory, base_quarantine_directory)

        # If the origin file is not a link.
        if not os.path.islink(save_directory):
            # Clean origin content.
            shutil.rmtree(save_directory, True)
            # Link the copied content in place of the origin.
            os.symlink(quarantine_directory, save_directory)

    def save_directory_quarantine_path(self, save_directory: str) -> str:
        return os.path.join(self.sandbox_path, 'save', os.path.basename(save_directory))

    def _load_registries_data(self) -> None:
        if os.path.exists(self.save_registries_quarantine_file):
            wineexec(
                "regedit",
                args="/S '%s'" % self.save_registries_quarantine_file,
                wine_path=self.get_executable(),
                prefix=self.prefix_path,
                arch=self.wine_arch,
                blocking=True,
            )

    def _save_registries_data(self) -> None:
        if self.registry_data_key:
            wineexec(
                "regedit",
                args="/E '%s' '%s'" % (self.save_registries_quarantine_file, self.registry_data_key),
                wine_path=self.get_executable(),
                prefix=self.prefix_path,
                arch=self.wine_arch,
                blocking=True,
            )

    # =============================== * DXVK * =============================== #
    def setup_dxvk(self):
        base_name: str = "dxvk"
        dxvk_manager: dxvk.DXVKManager = dxvk.DXVKManager(
            self.prefix_path,
            arch=self.wine_arch,
            version=self.runner_config.get("dxvk_version"),
        )

        try:
            self._toggle_dxvk(
                bool(self.runner_config.get(base_name)),
                dxvk_manager
            )
            self._setup_dxvk_dll_overrides(base_name)
        except dxvk.UnavailableDXVKVersion:
            raise GameConfigError("Unable to get %s %s" % (base_name.upper(), dxvk_manager.version))

    def _toggle_dxvk(self, enable: bool, dxvk_manager: dxvk.DXVKManager):
        # manual version only sets the dlls to native
        if dxvk_manager.version.lower() != "manual":
            if enable:
                if not dxvk_manager.is_available():
                    dxvk_manager.download()
                dxvk_manager.enable()
            else:
                dxvk_manager.disable()

        if enable:
            for dll in dxvk_manager.dxvk_dlls:
                # We have to make sure that the dll exists before setting it to native
                if dxvk_manager.dxvk_dll_exists(dll):
                    self.dll_overrides[dll] = 'native'

    def _setup_dxvk_dll_overrides(self, base_name: str):
        self.dll_overrides.update({
            'd3dcompiler': 'native',
            'd3dcompiler_33': 'native',
            'd3dcompiler_34': 'native',
            'd3dcompiler_35': 'native',
            'd3dcompiler_36': 'native',
            'd3dcompiler_37': 'native',
            'd3dcompiler_38': 'native',
            'd3dcompiler_39': 'native',
            'd3dcompiler_40': 'native',
            'd3dcompiler_41': 'native',
            'd3dcompiler_42': 'native',
            'd3dcompiler_43': 'native',
            'd3dcompiler_45': 'native',
            'd3dcompiler_46': 'native',
            'd3dcompiler_47': 'native'
        })

        self.dll_overrides.update({
            'd3dx9': 'native',
            'd3dx9_24': 'native',
            'd3dx9_25': 'native',
            'd3dx9_26': 'native',
            'd3dx9_27': 'native',
            'd3dx9_28': 'native',
            'd3dx9_29': 'native',
            'd3dx9_30': 'native',
            'd3dx9_31': 'native',
            'd3dx9_32': 'native',
            'd3dx9_33': 'native',
            'd3dx9_34': 'native',
            'd3dx9_35': 'native',
            'd3dx9_36': 'native',
            'd3dx9_37': 'native',
            'd3dx9_38': 'native',
            'd3dx9_39': 'native',
            'd3dx9_40': 'native',
            'd3dx9_41': 'native',
            'd3dx9_42': 'native',
            'd3dx9_43': 'native',

            'd3dx10': 'native',
            'd3dx10_33': 'native',
            'd3dx10_34': 'native',
            'd3dx10_35': 'native',
            'd3dx10_36': 'native',
            'd3dx10_37': 'native',
            'd3dx10_38': 'native',
            'd3dx10_39': 'native',
            'd3dx10_40': 'native',
            'd3dx10_41': 'native',
            'd3dx10_42': 'native',
            'd3dx10_43': 'native',

            'd3dx11': 'native',
            'd3dx11_42': 'native',
            'd3dx11_43': 'native'
        })

    # ======================== * Wine Main Services * ======================== #
    def get_env(self, os_env=True):
        """Return environment variables used by the game"""
        # Always false to runner.get_env, the default value
        # of os_env is inverted in the wine class,
        # the OS env is read later.
        env = super(wine, self).get_env(False)
        if os_env:
            env.update(os.environ.copy())
        show_debug = self.runner_config.get("show_debug", "-all")
        if show_debug != "inherit":
            env["WINEDEBUG"] = show_debug
        env["WINEARCH"] = self.wine_arch
        env["WINE"] = self.get_executable()
        env["WINEPREFIX"] = self.prefix_path
        logger.info("WINEPREFIX=%s", env["WINEPREFIX"])

        if not ("WINEESYNC" in env and env["WINEESYNC"] == "1"):
            env["WINEESYNC"] = "1" if self.runner_config.get("esync") else "0"

        overrides = self._get_dll_overrides()
        if overrides:
            env["WINEDLLOVERRIDES"] = get_overrides_env(overrides)
        return env

    def get_runtime_env(self):
        """Return runtime environment variables with path to wine for Lutris builds"""
        wine_path = self.get_executable()
        wine_root = None

        if WINE_DIR:
            wine_root = os.path.dirname(os.path.dirname(wine_path))

        if "-4." in wine_path or "/4." in wine_path:
            version = "Ubuntu-18.04"
        else:
            version = "legacy"

        return runtime.get_env(
            version=version,
            prefer_system_libs=self.system_config.get("prefer_system_libs", True),
            wine_path=wine_root,
        )

    def get_executable(self, version=None, fallback=True) -> Optional[str]:
        """Return the path to the Wine executable.
        A specific version can be specified if needed.
        """
        if version is None:
            version = self._get_version()
        if not version:
            return

        wine_path = self._get_path_for_version(version)
        if self.wine_arch == "win64":
            return wine_path + "64"
        else:
            return wine_path

    def is_installed(self, version=None, fallback=True, min_version=None):
        """Check if Wine is installed.
        If no version is passed, checks if any version of wine is available
        """
        if version:
            return system.path_exists(self.get_executable(version, fallback))

        wine_versions = get_wine_versions()
        if min_version:
            min_version_list, _, _ = parse_version(min_version)
            for wine_version in wine_versions:
                version_list, _, _ = parse_version(wine_version)
                if version_list > min_version_list:
                    return True
            logger.warning("Wine %s or higher not found", min_version)
        return bool(wine_versions)

    def setup_x360ce(self, x360ce_path):
        if not x360ce_path:
            return
        x360ce_path = os.path.expanduser(x360ce_path)
        if not os.path.isdir(x360ce_path):
            logger.error("%s is not a valid path for x360ce", x360ce_path)
            return
        mode = "dumbxinputemu" if self.runner_config.get("dumbxinputemu") else "x360ce"
        dll_files = ["xinput1_3.dll"]
        if self.runner_config.get("x360ce-xinput9"):
            dll_files.append("xinput9_1_0.dll")

        for dll_file in dll_files:
            xinput_dest_path = os.path.join(x360ce_path, dll_file)
            xinput_arch = self.runner_config.get("xinput-arch") or self.wine_arch
            dll_path = os.path.join(RUNTIME_DIR, mode, xinput_arch)
            if not system.path_exists(xinput_dest_path):
                source_file = dll_file if mode == "dumbxinputemu" else "xinput1_3.dll"
                shutil.copyfile(os.path.join(dll_path, source_file), xinput_dest_path)

        if mode == "x360ce":
            if self.runner_config.get("x360ce-dinput"):
                dinput8_path = os.path.join(dll_path, "dinput8.dll")
                dinput8_dest_path = os.path.join(x360ce_path, "dinput8.dll")
                shutil.copyfile(dinput8_path, dinput8_dest_path)

            x360ce_config = X360ce()
            x360ce_config.populate_controllers()
            x360ce_config.write(os.path.join(x360ce_path, "x360ce.ini"))

        # X360 DLL handling
        self.dll_overrides["xinput1_3"] = "native"
        if self.runner_config.get("x360ce-xinput9"):
            self.dll_overrides["xinput9_1_0"] = "native"
        if self.runner_config.get("x360ce-dinput"):
            self.dll_overrides["dinput8"] = "native"

    @classmethod
    def msi_exec(
        cls,
        msi_file,
        quiet=False,
        prefix=None,
        wine_path=None,
        working_dir=None,
        blocking=False,
    ):
        msi_args = "/i %s" % msi_file
        if quiet:
            msi_args += " /q"
        return wineexec(
            "msiexec",
            args=msi_args,
            prefix=prefix,
            wine_path=wine_path,
            working_dir=working_dir,
            blocking=blocking,
        )

    # ======================== * Wine Extra Services * ======================= #
    def _run_executable(self, executable):
        """Runs a Windows executable using this game's configuration"""
        wineexec(
            executable,
            wine_path=self.get_executable(),
            prefix=self.prefix_path,
            config=self,
            env=self.get_env(os_env=True),
        )

    def _get_dll_overrides(self):
        """Return the DLLs overriden at runtime"""
        try:
            overrides = self.runner_config["overrides"]
        except KeyError:
            overrides = {}
        else:
            if not isinstance(overrides, dict):
                logger.warning("DLL overrides is not a mapping: %s", overrides)
                overrides = {}
            else:
                overrides = overrides.copy()

        overrides.update(self.dll_overrides)
        return overrides

    def _get_version(self, use_default=True):
        """Return the Wine version to use. use_default can be set to false to
        force the installation of a specific wine version"""
        runner_version = self.runner_config.get("version")
        if runner_version:
            return runner_version
        if use_default:
            return get_default_version()

    def _get_path_for_version(self, version):
        """Return the absolute path of a wine executable for a given version"""
        # logger.debug("Getting path for Wine %s", version)
        if version in WINE_PATHS.keys():
            return system.find_executable(WINE_PATHS[version])
        if version == "custom":
            return self.runner_config.get("custom_wine_path", "")
        return os.path.join(WINE_DIR, version, "bin/wine")

    def _set_windows_version(self) -> None:
        winetricks(
            wine_path=self.get_executable(),
            prefix=self.prefix_path,
            arch=self.wine_arch,
            app="settings %s" % self.game_config.get("windows_version")
        )

    def _set_regedit_keys(self, prefix_manager: WinePrefixManager):
        """Reset regedit keys according to config."""
        # Those options are directly changed with the prefix manager and skip
        # any calls to regedit.
        managed_keys = {
            "UseXVidMode": prefix_manager.use_xvid_mode,
            "Desktop": prefix_manager.set_virtual_desktop,
            "WineDesktop": prefix_manager.set_desktop_size,
        }

        for key, path in self.reg_keys.items():
            value = self.runner_config.get(key) or "auto"
            if not value or value == "auto" and key not in managed_keys.keys():
                prefix_manager.clear_registry_subkeys(path, key)
            elif key in self.runner_config:
                if key in managed_keys.keys():
                    # Do not pass fallback 'auto' value to managed keys
                    if value == "auto":
                        value = None
                    managed_keys[key](value)
                    continue
                # Convert numeric strings to integers so they are saved as dword
                if value.isdigit():
                    value = int(value)

                prefix_manager.set_registry_key(path, key, value)

    def _configure_joypads(self, prefix_manager):
        if self.runner_config.get("autoconf_joypad", True):
            prefix_manager.configure_joypads()

    def _get_pids(self, wine_path=None):
        """Return a list of pids of processes using the current wine exe."""
        if wine_path:
            exe = wine_path
        else:
            exe = self.get_executable()
        if not exe.startswith("/"):
            exe = system.find_executable(exe)
        pids = system.get_pids_using_file(exe)
        if self.wine_arch == "win64" and os.path.basename(exe) == "wine":
            pids = pids | system.get_pids_using_file(exe + "64")

        # Add wineserver PIDs to the mix (at least one occurence of fuser not
        # picking the games's PID from wine/wine64 but from wineserver for some
        # unknown reason.
        pids = pids | system.get_pids_using_file(
            os.path.join(os.path.dirname(exe), "wineserver")
        )
        return pids
